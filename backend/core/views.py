from rest_framework import viewsets

from core.models import User, City, Message, Image, Address
from core.serializers import UserSerializer, CitySerializer, MessageSerializer, ImageSerializer, AddressSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class CityViewSet(viewsets.ModelViewSet):
    queryset = City.objects.all().order_by('-created_at')
    serializer_class = CitySerializer


class MessageViewSet(viewsets.ModelViewSet):
    queryset = Message.objects.all().order_by('-created_at')
    serializer_class = MessageSerializer


class ImageViewSet(viewsets.ModelViewSet):
    queryset = Image.objects.all().order_by('-created_at')
    serializer_class = ImageSerializer


class AddressViewSet(viewsets.ModelViewSet):
    queryset = Address.objects.all().order_by('-created_at')
    serializer_class = AddressSerializer
