from django.urls import include, path
from rest_framework import routers
from .views import JobViewSet, JobImageViewSet, CategoryViewSet, ReviewImageViewSet, \
    OrderViewSet, OrderCancellationViewSet, OfferViewSet, ReviewViewSet, CategoryVehicleTypeViewSet,\
    JobDetailViewSet

router = routers.DefaultRouter()
router.register(r'job-images', JobImageViewSet)
router.register(r'categories', CategoryViewSet)
router.register(r'review-images', ReviewImageViewSet)
router.register(r'orders', OrderViewSet)
router.register(r'order-cancellations', OrderCancellationViewSet)
router.register(r'offers', OfferViewSet)
router.register(r'reviews', ReviewViewSet)
router.register(r'category-vehicle-type', CategoryVehicleTypeViewSet)

urlpatterns = [
    path('jobs/', JobViewSet.as_view(), name='jobs'),
    path(r'jobs/<int:pk>', JobDetailViewSet, name='job_details'),
    path('', include(router.urls)),
]
