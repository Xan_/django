import React, { useEffect, useState, useContext } from 'react';
import { IonButton, IonCard, IonCol, IonRow, IonContent, IonModal, IonItemDivider, IonItem, IonInput, IonText, IonDatetime } from '@ionic/react';
import './NewOrder.css';
import { text } from 'ionicons/icons';
import { symlink } from 'fs';
import OrderPlacing from './OrderPlacing';
import OrderReady from './OrderReady';
import { useHistory } from 'react-router';
const axios = require('axios').default;

interface NewOrder {
  idCategory: string;
}


const NewOrder: React.FC<NewOrder> = ({ idCategory }) => {
  const [showModal, setShowModal] = useState(false);

  const Url = 'http://127.0.0.1:8000/';
  const apiUrlCategory = Url + "api/category-vehicle-type/";
  const apiUrlJobs = Url + "api/jobs/";

  var a = "";
  const [vehicleTypes, setVehicleTypes] = useState([]);

  useEffect(() => {
    fetch(`${apiUrlCategory}`).then(async response => {
      setVehicleTypes(await response.json());
    });
  }, []);

  const [idVehicle, setId] = useState<string>();
  const [commentDriver, setCommentDriver] = useState<string>();
  const [volume, setVolume] = useState<string>();
  const [weight, setWeight] = useState<string>();
  const [dateTime, setdateTime] = useState<string>();
  const [modalReady, setModalReady] = useState(false);


  let history = useHistory()


  const [style, setStyle] = useState<string>("");
  function SelectCar(id: string) {
    console.log(id);
    if (id !== idVehicle) {
      setStyle(`#bi${id}{
      border: 3px solid grey;
        }`);
      setId(id);
    } else {
      setStyle("");
      setId("");
    }
  }

  return (
    <div className="container-place-order">
      <style>
        {style}
      </style>
      <div>
        <IonRow>
          <IonCol size="1">
            <div className="position-div div-purple"> </div>
          </IonCol>
          <IonCol size="11">
            Kolizei
          </IonCol>
        </IonRow>
        <IonRow>
          <IonCol size="1">
            <div className="position-div div-yellow"> </div>
          </IonCol>
          <IonCol size="11">
            Отель
          </IonCol>
        </IonRow>
        <div className="scroolmenu">
          <div>
            {
              vehicleTypes
                .filter((vtype: any) => vtype.category.id == idCategory)
                .map((vtype: any) => {
                  const path = vtype.vehicle_type.image.url.split('/');
                  const nameImage = path[path.length - 1];
                  let activeClass = "";
                  return (
                    <IonCard id={"bi" + vtype.id} key={vtype.id} onClick={(e) => {
                      SelectCar(vtype.id);
                    }} className={"item-type " + activeClass}>
                      <img src={Url + 'media/img/' + nameImage} />
                      <br />
                      <p className="description-text">
                        {vtype.vehicle_type.name}
                      </p>
                      <p className="description-text">
                        {vtype.category.unit_price}
                      </p>
                    </IonCard>
                  );
                })
            }
          </div>
        </div>
        <IonRow>
          <IonCol size="6">
            <IonRow>
              <img className="payment-image" src="../../assets/icon/visa_PNG3.png" />
              .0120
            </IonRow>
          </IonCol>
          <IonCol size="6">
            <div className="wishes">
              <img src="../../assets/icon/plus.png" className="icon" />
              Пожелания
            </div>
          </IonCol>
        </IonRow>
        <section>

          <IonContent>
            <IonModal isOpen={showModal} cssClass='my-custom-class'>
              <IonText className="wish-margin"> <strong>Пожелания {idVehicle}</strong></IonText>
              <IonText className="wish-margin"> Укажите ваши пожелания и дополнительную информацию, если считаете её необходимой</IonText>
              <IonItem>
                <IonInput value={commentDriver} onIonChange={(e) => setCommentDriver((e.target as HTMLInputElement).value)} placeholder="Комментарии водителю" ></IonInput>
              </IonItem>
              <IonItem>
                <IonInput value={volume} onIonChange={(e) => setVolume((e.target as HTMLInputElement).value)} placeholder="Объем м&sup3;"></IonInput>
              </IonItem>
              <IonItem>
                <IonInput value={weight} onIonChange={(e) => setWeight((e.target as HTMLInputElement).value)} placeholder="Вес кг" ></IonInput>
              </IonItem>
              <IonItem>
                <IonDatetime value={dateTime} onIonChange={(e) => setdateTime((e.target as HTMLInputElement).value)} placeholder="Время и дата" display-timezone="utc"></IonDatetime>
              </IonItem>

              <IonButton color="primary" expand="block" className="place-button" onClick={() => {

                console.log("req body", idVehicle, commentDriver, volume, weight, dateTime);
                axios.post(apiUrlJobs, {
                  name: 'nameNety',
                  category: '',
                  description: '',
                  address: '',
                  quantity: volume,
                  date: '',
                  vehicle_type: idVehicle
                })
                  .then(function (res: any) {
                    console.log("res " + res);
                  })
                  .catch(function (error: any) {
                    console.log(error);
                  });
                setShowModal(false);
                setModalReady(true);
              }}>Готово</IonButton>


              <IonContent>
                <IonModal isOpen={modalReady} cssClass='my-custom-class'>
                  <OrderReady></OrderReady>
                  <IonButton color="primary" expand="block" className="place-button" onClick={() => {
                    history.goBack()
                  }
                  }>Вернуться в меню заказов</IonButton>
                </IonModal>
              </IonContent>

            </IonModal>
          </IonContent>
          <IonButton onClick={() => {
            if (!idVehicle) {
              console.log("Не выбран тип машины (ид)");
              return;
            } else {
              setShowModal(true)
            }
          }
          } color="primary" expand="block" className="place-button">Заказать</IonButton>
        </section>
      </div>
    </div>
  );
};

export default NewOrder;
