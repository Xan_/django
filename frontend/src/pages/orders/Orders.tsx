import {
  IonButtons, IonCard, IonCardContent, IonCardHeader,
  IonContent,
  IonHeader, IonImg, IonItem, IonList,
  IonMenuButton,
  IonPage,
  IonTitle,
  IonToolbar,
  IonGrid,
  IonCol,
  IonRow,
  IonButton,
  IonModal
} from '@ionic/react';
import React, { useEffect, useState } from 'react';
import './Orders.css';

import SegmentToggle from "../../components/SegmentToggle";
import CategorySelector from '../../components/CategorySelector';

const apiUrl = 'http://127.0.0.1:8000/api/jobs';


const Orders: React.FC<any> = ({history}: any) => {
  const [showModal, setShowModal] = useState(false);
  const segmentLabels = ['Активные', 'Завершенные'];
  const [activeSegment, setActiveSegment] = useState('');
  const onSegmentClicked = (label: string) => {
    setActiveSegment(label);
  };

  const [orders, setOrders] = useState([] as any[]);

  useEffect(() => {
    fetch(`${apiUrl}`).then(async response => {
      let orders = (await response.json()).map((order: any) => {
        return { ...order, date: new Date(order.date).toLocaleDateString() }
      });
      setOrders(orders);
    });
  }, [activeSegment]);


  return (

    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Заказы</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent>

        <div className="content-wrapper">

          <SegmentToggle segmentLabels={segmentLabels} activeLabel={activeSegment} onSegmentClicked={onSegmentClicked} />

          <IonCol>

            {orders.map(order => (

              <IonCard key={order.id}>
                <IonCardHeader>
                  <IonImg src="https://static-maps.yandex.ru/1.x/?ll=37.620070,55.753630&size=450,230&z=13&l=map&pt=37.620070,55.753630,pmwtm1~37.64,55.76363,pmwtm99
  " />
                </IonCardHeader>

                <IonCardContent>
                  <IonList>
                    <IonGrid >
                      <IonRow >
                        <IonCol size="8" >
                          <b className="text-date">
                            {order.date}
                          </b>
                        </IonCol>
                        <IonCol size="3">
                          <b className="text-price">
                            {order.category.unit_price * order.quantity}₽
                        </b>
                        </IonCol>
                      </IonRow>
                    </IonGrid>
                    <IonItem className="text-info">
                      <IonImg className="myicon" src="https://cdn4.iconfinder.com/data/icons/car-silhouettes/1000/sedan-512.png" />
                      {order.category.name}, {order.vehicle_type.name}
                    </IonItem>
                    <IonItem className="text-info">
                      <IonImg className="myiconm" src="https://cdn0.iconfinder.com/data/icons/small-n-flat/24/678111-map-marker-512.png" />
                      {order.address.full_address}
                    </IonItem>
                    {
                      order.dropoff &&
                      <IonItem className="text-info">
                        <IonImg className="myiconm" src="https://i.imgur.com/lDfYGot.png" />{order.dropoff.full_address}
                      </IonItem>
                    }

                    <section>
                      <IonButton color="warning" expand="block" className="details-button">Посмотреть предложение</IonButton>
                    </section>
                  </IonList>
                </IonCardContent>
              </IonCard>
            ))}

          </IonCol>

        </div>
        <div className="button-container">
          <IonModal isOpen={showModal}>
            <IonButton onClick={() => setShowModal(false)} className="add-order-button">
              <img className="icon" src="../../assets/icon/plus.png" />
              <p className="order-text">Создать заказ</p>
            </IonButton>
            <CategorySelector onItemClicked={categoryId => {
                history.push(`/place-order/${categoryId}`)
              }}
            />
            {/* <OrderPlaced></OrderPlaced> */}

          </IonModal>
          <IonButton className="add-order-button add-order-button-bottom" onClick={() => setShowModal(true)}>
            <img className="icon" src="../../assets/icon/plus.png" />
            <p className="order-text">Создать заказ</p>
          </IonButton>
        </div>
      </IonContent>
    </IonPage>
  );
};

export default Orders;
